//
//  Apply geodesic active contour method for liver segmentation
//  Follows the approach detailed in the Insight Software Guide
//  Code copied from ITK examples
//
//  INPUT: text file containing parameters to be read with libconfig
//  
//  Created on 2 February 2016
//  

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <libconfig.h++>
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkGeodesicActiveContourLevelSetImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"


int main(int argc, const char *argv[])
{
    // Check input filename provided
    if (argc < 2) {
        std::cerr << "Missing input filename! Usage:" << std::endl;
        std::cerr << argv[0] << " <config filename>" << std::endl;
    }
	
    libconfig::Config cfg;
    const unsigned int Dimension = 3;
	typedef float InputPixelType;
	typedef unsigned char OutputPixelType;
    typedef itk::Image< InputPixelType, Dimension > InputImageType;
	typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

    // Read the parameter file. If there is an error, report it and exit.
    try {
        cfg.readFile(argv[1]);
    }
    catch(const libconfig::FileIOException &fioex) {
        std::cerr << "I/O error while reading file." << std::endl;
        return(EXIT_FAILURE);
    }
    catch(const libconfig::ParseException &pex) {
        std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
                << " - " << pex.getError() << std::endl;
        return(EXIT_FAILURE);
    }
	
    ////////////////////////////////////////////////
    // 1) Read the input image

    std::string rwDir, inputImgName, outputImgName;
    try {
        rwDir = cfg.lookup("rwDir").c_str();
        inputImgName = cfg.lookup("inputImgName").c_str();
        outputImgName = cfg.lookup("outputImgName").c_str();
    }
    catch(const libconfig::SettingNotFoundException &nfex) {
        std::cerr << "Missing read/write directory, or input or output image";
        std::cerr << " filenames in configuration file." << std::endl;
        return(EXIT_FAILURE);
    }
	typedef itk::ImageFileReader< InputImageType > ReaderType;
	ReaderType::Pointer reader = ReaderType::New();
    std::string readpath(rwDir);
	readpath.append(inputImgName);
	reader->SetFileName( readpath );
	reader->Update();
	std::cout << "The input path is:" << std::endl;
	std::cout << reader->GetFileName() << std::endl;
	
    ////////////////////////////////////////////////
    // 2) Curvature anisotropic diffusion
	
	typedef itk::CurvatureAnisotropicDiffusionImageFilter< 
		InputImageType, InputImageType > SmoothingFilterType;
	SmoothingFilterType::Pointer smoothing = SmoothingFilterType::New();
	smoothing->SetTimeStep(0.04);
	smoothing->SetNumberOfIterations(5);
	smoothing->SetConductanceParameter(9.0);
	smoothing->SetInput( reader->GetOutput() );
	
    ////////////////////////////////////////////////
    // 3) Gradient magnitude recursive Gaussian
	
	float sigma;
    try {
        sigma = cfg.lookup("sigma");
    }
    catch(const libconfig::SettingNotFoundException &nfex) {
        std::cerr << "Unable to read sigma!" << std::endl;
        return(EXIT_FAILURE);
    }
	typedef itk::GradientMagnitudeRecursiveGaussianImageFilter< 
		InputImageType, InputImageType > GradientFilterType;
	GradientFilterType::Pointer gradientMagnitude = GradientFilterType::New();
	gradientMagnitude->SetSigma( sigma );
	gradientMagnitude->SetInput( smoothing->GetOutput() );
	std::cout << "Sigma = " << gradientMagnitude->GetSigma() << std::endl;
	
    ////////////////////////////////////////////////
    // 4) Sigmoid mapping
	
	float K1, K2;
	try {
		K1 = cfg.lookup("sigmoid.K1");
		K2 = cfg.lookup("sigmoid.K2");
	}
	catch(const libconfig::SettingNotFoundException &nfex) {
		std::cerr << "Unable to read sigmoid mapping parameters!" << std::endl;
		return(EXIT_FAILURE);
	}
	typedef itk::SigmoidImageFilter< InputImageType, InputImageType > 
		SigmoidFilterType;
	SigmoidFilterType::Pointer sigmoid = SigmoidFilterType::New();
	sigmoid->SetOutputMinimum(0.0);
	sigmoid->SetOutputMaximum(1.0);
	sigmoid->SetAlpha( (K2 - K1)/6 );
	sigmoid->SetBeta( (K1 + K2)/2 );
	sigmoid->SetInput( gradientMagnitude->GetOutput() );
	std::cout << "alpha = " << sigmoid->GetAlpha();
	std::cout << ", beta = " << sigmoid->GetBeta() << std::endl;

    ////////////////////////////////////////////////
    // 5) Input level set with fast marching
	
	const libconfig::Setting& root = cfg.getRoot();
	const libconfig::Setting &seeds = root["seeds"];
	int numSeeds = seeds.getLength();
	std::cout << "# of seeds: " << numSeeds << std::endl;
	int* coords = new int[3*numSeeds];
	int i;
	for (i = 0; i < numSeeds; i++) {
		const libconfig::Setting &seed = seeds[i];
		if(!(seed.lookupValue("x", coords[3*i])
			&& seed.lookupValue("y", coords[3*i+1])
			&& seed.lookupValue("z", coords[3*i+2])))
			continue;
	}
	std::cout << "The seeds are:" << std::endl;
	for (i = 0; i < numSeeds; i++) {
		std::cout << "(1st seed) x=";
		std::cout << coords[3*i];
		std::cout << ", y=" << coords[3*i+1];
		std::cout << ", z=" << coords[3*i+2] << std::endl;
	}
	
	typedef itk::FastMarchingImageFilter< InputImageType, InputImageType > 
		FastMarchingFilterType;
	FastMarchingFilterType::Pointer fastMarching = FastMarchingFilterType::New();
	typedef FastMarchingFilterType::NodeContainer NodeContainer;
	typedef FastMarchingFilterType::NodeType NodeType;
	
	NodeContainer::Pointer seedCoords = NodeContainer::New();
	InputImageType::IndexType seedPosition;
	seedPosition[0] = coords[0];
	seedPosition[1] = coords[1];
	seedPosition[2] = coords[2];
	//const double initialDistance = atof( argv[7] );
	const double initialDistance = 10.0;
	const double seedValue = -initialDistance;
	NodeType node;
	node.SetValue( seedValue );
	node.SetIndex( seedPosition );
	seedCoords->Initialize();
	seedCoords->InsertElement(0, node);
	
	fastMarching->SetTrialPoints( seedCoords );
	std::cout << std::endl;
	NodeContainer::Pointer seedCheck = fastMarching->GetTrialPoints();
	std::cout << seedCheck->ElementAt(0).GetIndex() << std::endl;
	/*
	fastMarching->SetSpeedConstant(1.0);
	fastMarching->SetOutputSize( reader->GetOutput()->GetBufferedRegion().GetSize() );
	fastMarching->SetOutputRegion( reader->GetOutput()->GetBufferedRegion() );
	fastMarching->SetOutputSpacing( reader->GetOutput()->GetSpacing() );
	fastMarching->SetOutputOrigin( reader->GetOutput()->GetOrigin() );
	
	////////////////////////////////////////////////
    // 5) Segmentation with geodesic active contour
		
	typedef itk::GeodesicActiveContourLevelSetImageFilter< 
		InputImageType, InputImageType > GeodesicActiveContourFilterType;
	GeodesicActiveContourFilterType::Pointer geodesicActiveContour = 
		GeodesicActiveContourFilterType::New();
	
	const double propagation = atof( argv[11] );
	const double curvature = atof( argv[12] );
	const double advection = atof( argv[13] );
	const double iterations = atoi( argv[14] );
	geodesicActiveContour->SetPropagationScaling( propagation );
	geodesicActiveContour->SetCurvatureScaling( curvature );
	geodesicActiveContour->SetAdvectionScaling( advection );
	geodesicActiveContour->SetMaximumRMSError(0.01);
	geodesicActiveContour->SetNumberOfIterations( iterations );
	
	geodesicActiveContour->SetInput( fastMarching->GetOutput() );
	geodesicActiveContour->SetFeatureImage( sigmoid->GetOutput() );
	
    ////////////////////////////////////////////////
    // 6) Binary thresholding
	
	typedef itk::BinaryThresholdImageFilter< InputImageType, OutputImageType > 
		ThresholdingFilterType;
	ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
	thresholder->SetLowerThreshold(-1000.0);
	thresholder->SetUpperThreshold(0.0);
	thresholder->SetOutsideValue(0);
	thresholder->SetInsideValue(255);
	thresholder->SetInput( geodesicActiveContour->GetOutput() );
	
	////////////////////////////////////////////////
    // 7) Write output image
	
	typedef itk::ImageFileWriter< OutputImageType > WriterType;
	WriterType::Pointer writer = WriterType::New();
	std::string writepath( argv[1] );
	writepath.append( argv[3] );
	writer->SetFileName( writepath );
	writer->SetInput( thresholder->GetOutput() );
	
	try {
		writer->Update();
	}
	catch( itk::ExceptionObject &excep ) {
		std::cerr << "Exception caught!" << std::endl;
		std::cerr << excep << std::endl;
		return EXIT_FAILURE;
	}
	
	// The following writer is used to save the output of the sigmoid mapping
	typedef itk::ImageFileWriter< InputImageType > InternalWriterType;
	InternalWriterType::Pointer speedWriter = InternalWriterType::New();
	speedWriter->SetInput( sigmoid->GetOutput() );
	std::string sigmoidpath( argv[1] );
	speedWriter->SetFileName( sigmoidpath.append("SigmoidForGeodesic.mha"));
	speedWriter->Update();
	*/
	return 0;
}
